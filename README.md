# Projeto Sistema Distribuído Parte App - Sistemas Distribuídos


### Tecnologias Utilizadas 

*  Framework React Native
*  Javascript

### Requisitos 

*  NodeJs 10 ou superior. 
*  Python 2. 
*  JDK 8. 
*  Android Studio.

### Como começar

* Acesse o link https://facebook.github.io/react-native/docs/getting-started, nele tem todo o passo a passo das instalações de todos os requisitos.

#### Como rodar a aplicação passo a passo

*  Após acessar o link acima e configurar tudo, caso não tenha na sua máquina baixe e instale um editor de texto (recomendo o **VSCode**, na minha opinião um dos melhores gratuitos disponíveis e a instalação também é simples);
*  Abra o editor de texto.
*  No editor abra o projeto ime-app.
*  Configure o emulador seguindo os passos disponibilizados no link.
*  Pressione **control** e **apóstrofo** para abrir o terminal direto pelo editor (você pode fazer isso pelo cmd do windowns se preferir).
*  Digite o comando **react-native run-adroid** para instalar o aplicativo no emulador, nas próximas veze basta digitar **react-native start**.
*  Para testar por completo o projeto você deve abrir uma nova janela no editor e abrir o projeto ime-web.
*  Startar o MySQL e o Apache no XAMP e então o projeto ime-web no editor através do comando **php artisan serve**.

### Licença Utilizada

*  Mit License
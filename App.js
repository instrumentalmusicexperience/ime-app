import React, { Component } from 'react'
import { Button, Text, StyleSheet, View, FlatList, Image, TextInput, ScrollView } from 'react-native'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'


class TelaPrincipal extends Component {
  static navigationOptions = {
    title: 'Intrumental Music Experience',
    headerStyle: {
      backgroundColor: 'red',
      textAlign: 'center'
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      intrumentos: [],
      url: "http://10.0.2.2:8000/api/produtosapi"
    };
  }
  componentDidMount() {
    this.loadIntrumentos();
  }

  loadIntrumentos = () => {
    fetch(this.state.url)
      .then(response => response.json())
      .then((response) => {
        this.setState({
          intrumentos: response
        })
      })
      .catch(error => console.log(error))
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.border}>
          <Text>Conheça, teste e aprove antes de comprar!</Text>
        </View>
        <View style={styles.logo}>
          <Image source={require('./logoIME.jpg')} style={{ width: 150, height: 100 }} />
        </View>
        <FlatList
          data={this.state.intrumentos}
          renderItem={
            ({ item }) =>
              <View style={styles.view}>
                <Image source={require('./guitarra.jpg')} style={{ width: 350, height: 350 }} />
                <Text style={{ margin: 3, fontWeight: 'bold' }}>Código do Instrumento: {item.id}</Text>
                <Text style={{ margin: 3, }}>Categoria: {item.categoria}</Text>
                <Text style={{ margin: 3, }}>Modelo: {item.modelo}</Text>
                <Text style={{ margin: 3, }}>Marca: {item.marca}</Text>
                <View style={{ margin: 20, }}>
                  <Button
                    title='Reservar'
                    color='red'
                    onPress={() => this.props.navigation.navigate('SegundaTela')}
                  />
                </View>
              </View>

          }
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}



class TelaReservas extends Component {
  static navigationOptions = {
    title: 'Intrumental Music Experience',
    headerStyle: {
      backgroundColor: 'red',
      justifyContent: 'center'

    }

  }

  constructor(props) {
    super(props);
    this.state = {
      produto_id: '',
      nomeCliente: '',
      emailCliente: '',
      telefone: '',
      data: '',
      resposta: '',
    };
  }

  enviaDados = () => {

    try {

      fetch('http://10.0.2.2:8000/reservasapi', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          produto_id: this.state.produto_id,
          nomeCliente: this.state.nomeCliente,
          emailCliente: this.state.emailCliente,
          telefone: this.state.telefone,
          data: this.state.data,
        }),
      })

      this.setState({ resposta: 'Solicitação enviada, você recebera um email de confirmação!' })

    } catch (error) {
      console.error(error)

      this.setState({ resposta: "Erro ao solicitar, tente mais tarde!" })
    }

  }


  render() {

    return (
      <ScrollView>
        <View>
          <View style={styles.logo}>
            <Image source={require('./logoIME.jpg')} style={{ width: 150, height: 100 }} />
          </View>
          <View>
            <Text style={{ fontSize: 16, margin: 4, marginTop: 20 }}>Preencha os dados Abaixo</Text>
          </View>
          <View style={styles.inputs}>
            <TextInput placeholder="Informe o Códido do Instrumento"
              style={{ borderWidth: 0.5, borderColor: 'red', marginTop: 10 }} onChangeText={(produto_id) => this.setState({ produto_id })} />
            <TextInput placeholder="Seu Nome"
              style={{ borderWidth: 0.5, borderColor: 'red', marginTop: 10 }} onChangeText={(nomeCliente) => this.setState({ nomeCliente })} />
            <TextInput placeholder="Email"
              style={{ borderWidth: 0.5, borderColor: 'red', marginTop: 10 }} onChangeText={(emailCliente) => this.setState({ emailCliente })} />
            <TextInput placeholder="Telefone"
              style={{ borderWidth: 0.5, borderColor: 'red', marginTop: 10 }} onChangeText={(telefone) => this.setState({ telefone })} />
            <TextInput placeholder="Data desejada"
              style={{ borderWidth: 0.5, borderColor: 'red', marginTop: 10 }} onChangeText={(data) => this.setState({ data })} />
          </View>
          <View style={{ margin: 20, }}>
            <Button
              title='Reservar'
              color='red'
              onPress={this.enviaDados}
            />
          </View>
          <View style={{ fontSize: 20, flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ color: 'blue' }}>
              {this.state.resposta}
            </Text>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10
  },

  border: {
    flexDirection: "row",
    justifyContent: 'center',
  },

  logo: {
    flexDirection: "row",
    justifyContent: 'center',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'red',
    margin: 2
  },

  view: {
    borderRadius: 4,
    borderWidth: 2,
    borderColor: 'red',
    margin: 2
  },
  inputs: {
    flexDirection: 'column',
    margin: 4,
  }

});


const AppNavigator = createStackNavigator(
  {
    PrimeiraTela: {
      screen: TelaPrincipal
    },
    SegundaTela: {
      screen: TelaReservas
    }
  },
  {
    initialRouteName: 'PrimeiraTela'
  }
);

const AppContainer = createAppContainer(AppNavigator);


export default class App extends Component {

  render() {
    return <AppContainer />;
  }
}


